var express = require('express');
var router = express.Router();
var Users = require('../model/User');

router.get('/', function(req, res, next) {
  Users.find({}, function (err, users) {
    if (err) { next(err) }
    res.json(users);
  });
});

router.get('/:id', function(req, res, next) {
  var id = req.params.id;
  Users.findById(id, function (err, user) {
    if (err) { return next(err); }
    if (!user) { return res.status(404).send("User not found"); }
    res.json(user);
  });
});

module.exports = router;
