var express = require('express');
var router = express.Router();
// var HttpError = require('error').HttpError;
var AuthError = require('../model/User').AuthError;
var User = require('../model/User');
var checkAuth = require('../middleware/checkAuth');

router.get('/', function(req, res, next) {
    if (!req.session.user) {
        return res.redirect('/login');
    } else {
        res.render('index', { user: req.user});
    }
});

router.get('/login', function (req, res, next) {
  res.render('login');
});
router.post('/login', function (req, res, next) {
    var username = req.body.username,
        password = req.body.password;
    User.authorize(username, password, function(err, user) {
        if (err) {
            if (err instanceof AuthError) {
                return res.status(403).json({message: "User not authorized. " + err.message});
            } else {
                return next(err);
            }
        }
        req.session.user = user._id;
        res.send({});
    });
});

router.post('/logout', function (req, res, next) {
    var sid = req.session.id;

    var io = req.app.get('io');
    req.session.destroy(function(err) {
        // io.sockets.$emit("session:reload", sid);
        if (err) return next(err);

        res.redirect('/');
    });
});

module.exports = router;
