var connect = require('connect'); // npm i connect
var async = require('async');
var cookie = require('cookie');   // npm i cookie
var sessionStore = require('../lib/sessionStore');
var HttpError = require('../error/index').HttpError;
var User = require('../model/User');

function loadSession(sid, callback) {

    // sessionStore callback is not quite async-style!
    sessionStore.load(sid, function(err, session) {
        if (arguments.length == 0) {
            // no arguments => no session
            return callback(null, null);
        } else {
            return callback(null, session);
        }
    });

}

function loadUser(session, callback) {
    if (!session.user) {
        return callback(null, null);
    }
    User.findById(session.user, function(err, user) {
        if (err) return callback(err);

        if (!user) {
            return callback(null, null);
        }
        callback(null, user);
    });

}


module.exports = function (server, io) {
    var cookieParser = require('cookie-parser');
    var allClients = [];

    io.use(function(socket, next) {
        if (!socket) {
            next(new Error('not authorized'))
        }
        async.waterfall([
            function(callback) {
                socket.handshake.cookies = cookie.parse(socket.handshake.headers.cookie || '');
                var sidCookie = socket.handshake.cookies['connect.sid'];
                var sid = cookieParser.signedCookie(sidCookie, 'keyboard cat');
                loadSession(sid, callback);
            },
            function(session, callback) {
                if (!session) {
                    callback(new HttpError(401, "No session"));
                }
                socket.handshake.session = session;
                loadUser(session, callback);
            },
            function(user, callback) {
                if (!user) {
                    callback(new HttpError(403, "Anonymous session may not connect"));
                }
                socket.handshake.user = user;
                callback(null);
            }
        ], function(err) {
            if (!err) {
                return next();
            }
            if (err instanceof HttpError) {
                return next(null, false);
            }
            return next(err);
        });
    });

    io.on('session:reload', function(sid) {
        var clients = io.sockets.clients();

        clients.forEach(function(client) {
            if (client.handshake.session.id != sid) return;

            loadSession(sid, function(err, session) {
                if (err) {
                    client.emit("error", "server error");
                    client.disconnect();
                    return;
                }
                if (!session) {
                    client.emit("logout");
                    client.disconnect();
                    return;
                }
                client.handshake.session = session;
            });
        });
    });
    var users = [];
    io.on('connection', function(socket) {
        socket.join('all');
        var user = socket.handshake.user;
        socket.broadcast.emit('join', user.username);
        var USER = {
            x: 0,
            y: 0,
            name: user.username
        };

        var clients = io.sockets.adapter.rooms['all'].sockets;
        var numClients = (typeof clients !== 'undefined') ? Object.keys(clients).length : 0;
        for (var clientId in clients ) {
            // console.log(clients);
            var clientSocket = io.sockets.connected[clientId];
            console.log('----------------');
            console.log(clientSocket.handshake.user);
            if (users.indexOf(clientSocket.handshake.user.username) === -1) {
                console.log(users.indexOf(USER.name));
                users.push(clientSocket.handshake.user.username);
            }
            clientSocket.emit('getUsers', users);
        }

        socket.on('fire', function (coordinates) {
            socket.broadcast.emit('enemyFire', coordinates);
        });
        
        socket.on('userMove', function (coordinates) {
            socket.broadcast.emit('enemyMove', coordinates);
        });

        socket.on('message', function(text, cb) {
            socket.broadcast.emit('message', user.username, text);
            cb();
        });

        socket.on('disconnect', function() {
            if (users.indexOf(USER.name) > -1) {
                users.splice(users.indexOf(USER.name), 1);
            }
            socket.broadcast.emit('leave', user.username);
        });

        socket.on('typing', function(user) {
            socket.broadcast.emit('typeUser', user.username);
        });
    });

    return io;
};